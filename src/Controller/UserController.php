<?php

namespace App\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
class UserController extends AbstractController {

    /**
     * @Route("register", methods={"POST"})
     */
    function CreateUser(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $password = random_int( 1000001 , 9999999 );
        $aleatoireInt = random_int( 1000001 , 9999999 );
        $aleatoireString = chr(rand(97 , 122));
        $identifiant = '12345'.$aleatoireInt.$aleatoireString ;
        $user = new User();
        $user->setCivilites($request->get('civilites'));
        $user->setPrenom($request->get('prenom'));
        $user->setNom($request->get('nom'));
        $user->setdateNaissance(date_create_from_format('Y-m-d', $request->get('dateNaissance')));
        $user->setAdresse($request->get('adresse'));
        $user->setIdentifiant($identifiant);
        $user->setMontant(0);
        $user->setPassword($password);
        $entityManager->persist($user);
        $entityManager->flush();
        $returnable = array('Code pin' => $password, 'Identifiant' => $identifiant);
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($returnable, 'json');

        return new Response($jsonContent, Response::HTTP_CREATED);
    }





    /**
     * @Route("connexion", methods={"POST"})
     */
    function GenerateJWT(Request $request)
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);

        $user = $userRepository->findByIdentifiant($request->get('login'));

        if($user[0]->getIdentifiant() == $request->get('login') && $user[0]->getPassword() == $request->get('password'))
        {
            $user_data = array(
                'login' => $user[0]->getIdentifiant(),
                'password' => $user[0]->getPassword(),
                'date' => date('Y-m-d')
            );
            $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

            $payload = json_encode($user_data);

            $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
            $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

            $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', true);

            $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

            $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

            
        }

        $_SESSION["jwt"] = $jwt;
        $returnable = array('jwt' => $jwt);
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($returnable, 'json');

        return new Response($jsonContent, Response::HTTP_CREATED);
    }



     /**
     * @Route("deconnexion", methods={"DELETE"})
     */
    function deconnexion(Request $request)
    {
        if(isset($_SESSION['jwt'])){
            unset($_SESSION['jwt']);
        }
        return new Response("", Response::HTTP_OK, ['content-type' => 'application/json']);
    }

    }


?>